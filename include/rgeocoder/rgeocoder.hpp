#pragma once

#include "geocode.hpp"

#include <string>

/**
 * @namespace rgeocoder
 * @brief  Namespace containing all the functions and types for Reverse 
 * Geocoding.
 */
namespace rgeocoder
{
    extern const std::string VERSION;

    /**
     * @brief  Finds the nearest Geocode to the provided coordinates.
     * @note   It is guaranteed that there will always be a single closest point.
     * @param  latitude: the latitude of the point to search at (in decimal degrees).
     * @param  longitude: the longitude of the point to search at (in decimal degrees).
     * @return A Geocode struct instance represeting the closest geocode point.
     */
    rgeocoder::Geocode getNearestGeocode(double latitude, double longitude);

    /**
     * @brief  Attempts to find the name of the country referred to by the provided
     * country code string.
     * @note   Returns an empty string if the country code isn't found.
     * @param  &country_code: the country code to search for.
     * @retval The name of the country as a string if found, otherwise an empty
     * string.
     */
    std::string countryCodeToName(const std::string &country_code);

    /**
     * @brief  Performs the Haversine calculation to determine the distance (in
     * metres) between the two sets of coordinates provided.
     * @param  latitude_1:  the latitude (in decimal degrees) of the first point.
     * @param  longitude_1: the longitude (in decimal degrees) of the first point.
     * @param  latitude_2:  the latitude (in decimal degrees) of the second point.
     * @param  longitude_2: the longitude (in decimal degrees) of the second point.
     * @return The distance between the two sets of points in metres, as a double.
     */
    double distanceBetweenMetres(const double latitude_1, const double longitude_1, const double latitude_2, const double longitude_2);

    /**
     * @brief  Performs the Haversine calculation to determine the distance (in
     * metres) between the two Geocode points provides.
     * @param  &point_1: The first Geocode point.
     * @param  &point_2: The second Geocode point.
     * @return The distance between the two Geocode points in metres, as a double.
     */
    double distanceBetweenMetres(const rgeocoder::Geocode &point_1, const rgeocoder::Geocode &point_2);
}