#pragma once

#include <string>

namespace rgeocoder
{
    /**
     * @struct Geocode
     * @brief  Struct representing a single geocode point.
     * 
     * The Geocode struct contains all the discernible information about a geocode
     * location, such as coordinates, administration names, country code and
     * timezone. 
     * @note   Admin codes are not guaranteed to be filled in for every location.
     */
    struct Geocode 
    {
        double latitude;
        double longitude;
        std::string name;
        std::string admin_1;
        std::string admin_2;
        std::string country_code;
        std::string timezone;
    };
}