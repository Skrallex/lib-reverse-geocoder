# General CPack settings
set(CPACK_PACKAGE_CONTACT "Skrallex")
set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.com/Skrallex/lib-reverse-geocoder")

# Package long description
set(DEBIAN_PACKAGE_LONG_DESCRIPTION
" This package provides the reverse-geocoder library, which is a Linux C++ shared\n\
 library that can be used to produce a geocode location\n\
 from a set of coordinates. An internet connection is not required as the\n\
 data is a part of the library.\n\
 .\n\
 The library provides a simple interface that can lookup the nearest geocode\n\
 location. The information returned includes: country code, country name,\n\
 administration level 1 (eg. state), administration level 2 (eg. city),\n\
 and the timezone (not inclusive of daylight-savings timezones).")

# Settings for .deb packaging
if(CPACK_GENERATOR MATCHES "DEB")
	set(CPACK_DEBIAN_PACKAGE_DEPENDS "libc6 (>= 2.2.5), libgcc1 (>= 1:3.0), libstdc++6 (>= 4.8)")
	set(CPACK_DEBIAN_PACKAGE_SECTION "libs")
	set(CPACK_DEBIAN_PACKAGE_PRIORITY "optional")
	#set(CPACK_DEBIAN_PACKAGE_SHLIBDEPS "ON")
    set(CPACK_DEBIAN_PACKAGE_HOMEPAGE "https://gitlab.com/Skrallex/lib-reverse-geocoder")
	set(CPACK_DEBIAN_PACKAGE_SUGGESTS "reverse-geocoder-cli")
	set(CPACK_DEBIAN_PACKAGE_DESCRIPTION "${CPACK_PACKAGE_DESCRIPTION_SUMMARY}\n${DEBIAN_PACKAGE_LONG_DESCRIPTION}")
	set(CPACK_DEBIAN_FILE_NAME "DEB-DEFAULT")
endif()

if(CPACK_GENERATOR MATCHES "TGZ")
endif()