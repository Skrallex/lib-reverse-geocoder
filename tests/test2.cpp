#include "test.hpp"

// Sydney test
int main()
{
    rgeocoder::Geocode closest = rgeocoder::getNearestGeocode(-33.856386, 151.2141948);
    
    // std::cout << "cc: " << closest.country_code << " admin1: " << closest.admin_1 <<  " admin2: " << closest.admin_2 << " country: " << rgeocoder::countryCodeToName(closest.country_code) << std::endl;

    // Check the country code matches
    testEqual(closest.country_code, "AU", __FILE__, __LINE__);

    // Check the country name matches
    testEqual(rgeocoder::countryCodeToName(closest.country_code), "Australia", __FILE__, __LINE__);

    // Check the admin1 matches
    testEqual(closest.admin_1, "New South Wales", __FILE__, __LINE__);

    exit(EXIT_SUCCESS);
}