#pragma once

#include "../include/rgeocoder/rgeocoder.hpp"

#include <cstdlib>
#include <iostream>
#include <sstream>

void exit_fail(const char* file_name, unsigned int line_number, const std::string &message)
{
    // Print the file name, line number and formatted message
    std::cout << " > Test failed: " << message << " (" << file_name << ":" << line_number << ")" << std::endl;
    exit(EXIT_FAILURE);
}

template<class T, class U> bool testEqual(T one, U two, const char* file_name, unsigned int line_number)
{
    if(one != two)
    {
        std::stringstream ss;

        ss << "'" << one << "' != '" << two << "'";

        #ifndef TESTS_NO_EXIT
        exit_fail(file_name, line_number, ss.str());
        #endif

        return false;
    }

    return true;
}

template<class T, class U> bool testNotEqual(T one, U two, const char* file_name, unsigned int line_number)
{
    if(one == two)
    {
        std::stringstream ss;

        ss << "'" << one << "' != '" << two << "'";

        #ifndef TESTS_NO_EXIT
        exit_fail(file_name, line_number, ss.str());
        #endif

        return false;
    }
    return true;
}