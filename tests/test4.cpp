#include "test.hpp"

// London test
int main()
{
    rgeocoder::Geocode closest = rgeocoder::getNearestGeocode(51.5265482, -0.1282007);

    // std::cout << "cc: " << closest.country_code << " admin1: " << closest.admin_1 <<  " admin2: " << closest.admin_2 << " country: " << rgeocoder::countryCodeToName(closest.country_code) << std::endl;

    // Check the country code matches
    testEqual(closest.country_code, "GB", __FILE__, __LINE__);

    // Check the country name matches
    testEqual(rgeocoder::countryCodeToName(closest.country_code), "United Kingdom", __FILE__, __LINE__);

    // Check the admin1 matches
    testEqual(closest.admin_1, "England", __FILE__, __LINE__);

    // Check the admin2 matches
    testEqual(closest.admin_2, "Greater London", __FILE__, __LINE__);

    exit(EXIT_SUCCESS);
}