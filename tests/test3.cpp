#include "test.hpp"

// New York test
int main()
{
    rgeocoder::Geocode closest = rgeocoder::getNearestGeocode(40.7789578, -73.9737418);

    // std::cout << "cc: " << closest.country_code << " admin1: " << closest.admin_1 <<  " admin2: " << closest.admin_2 << " country: " << rgeocoder::countryCodeToName(closest.country_code) << std::endl;

    // Check the country code matches
    testEqual(closest.country_code, "US", __FILE__, __LINE__);
    
    // Check the country name matches
    testEqual(rgeocoder::countryCodeToName(closest.country_code), "United States", __FILE__, __LINE__);

    // Check the admin1 matches
    testEqual(closest.admin_1, "New York", __FILE__, __LINE__);

    // Check the admin2 matches
    testEqual(closest.admin_2, "New York County", __FILE__, __LINE__);

    exit(EXIT_SUCCESS);
}