#define TESTS_NO_EXIT
#include "test.hpp"

// Test the tests
int main()
{
    // Int <-> Int
    if(testEqual(1, 2, __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'1' == '2'");

    if(!testEqual(1, 1, __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'1' != '1'");

    if(testNotEqual(1, 1, __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'1' != '1'");

    if(!testNotEqual(1, 2, __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'1' == '2'");

    // string <-> string
    if(testEqual("abcdefg", "gfedcba", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'abcdefg' == 'gfedcba'");

    if(!testEqual("abcdefg", "abcdefg", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'abcdefg' != 'abcdefg'");

    if(testNotEqual("abcdefg", "abcdefg", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'abcdefg' != 'abcdefg'");

    if(!testNotEqual("abcdefg", "gfedcba", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'abcdefg' == 'gfedcba'");

    // string <-> char*
    if(testEqual(std::string("hello"), "goodbye", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'hello' == 'goodbye'");

    if(!testEqual(std::string("hello"), "hello", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'hello' != 'hello'");

    if(testNotEqual(std::string("hello"), "hello", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'hello' != 'hello'");

    if(!testNotEqual(std::string("hello"), "goodbye", __FILE__, __LINE__))
        exit_fail(__FILE__, __LINE__, "'hello' == 'goodbye'");

    exit(EXIT_SUCCESS);
}