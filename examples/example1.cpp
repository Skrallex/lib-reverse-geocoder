#include "../include/rgeocoder/rgeocoder.hpp"

#include <cstdlib>
#include <iostream>

// Compile with: g++ example1.cpp -lrgeocoder -o basic_example.out

int main(void)
{
    // Example 1 - Sydney
    double example_latitude = -33.856386;
    double example_longitude = 151.2141948;

    // Find the closest point
    rgeocoder::Geocode closest_geocode = rgeocoder::getNearestGeocode(example_latitude, example_longitude);

    // Get the distance from the Geocode
    double distance_from = rgeocoder::distanceBetweenMetres(example_latitude, example_longitude, closest_geocode.latitude, closest_geocode.longitude);

    // Print the distance
    std::cout << distance_from << " m away from ";

    // Print the locality name
    std::cout << closest_geocode.name;

    // Print the administration level 2 name
    std::cout << ", " << closest_geocode.admin_2;

    // Print the administration level 1 name
    std::cout << ", " << closest_geocode.admin_1;

    // Print the country name from the country code
    std::cout << ", " << rgeocoder::countryCodeToName(closest_geocode.country_code);

    // Print the timezone
    std::cout << " (TZ: " << closest_geocode.timezone << ")";

    std::cout << std::endl;

    exit(0);
}