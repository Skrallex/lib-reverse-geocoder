#include "../include/rgeocoder/rgeocoder.hpp"

#include "country_name_map.hpp"
#include "geocodes/geocode_parts.hpp"

#include <cmath>

// Make-file #def
#ifndef AUTO_FILL_VERSION
    #define AUTO_FILL_VERSION ""
#endif

namespace rgeocoder
{
    // Variables
    const std::string VERSION = AUTO_FILL_VERSION;
    const static double earth_radius_m = 6372800;

    // Converts degrees to radians
    inline double deg2Rad(const double degrees)
    {
        return M_PI * degrees / 180.0;
    }

    rgeocoder::Geocode getNearestGeocode(double latitude, double longitude)
    {
        Geocode closest;
        double closest_distance = INFINITY;

        // Loop through all the Geocode parts
        for(const auto &geocode_part: rgeocoder::parts::geocodes)
        {
            // Loop through each Geocode in the part
            for(const auto &geocode: *geocode_part)
            {
                // Check if the geocode point is closer
                double distance = distanceBetweenMetres(latitude, longitude, geocode.latitude, geocode.longitude);
                if(distance < closest_distance)
                {
                    closest = geocode;
                    closest_distance = distance;
                }
            }
        }

        return closest;
   }

   std::string countryCodeToName(const std::string &country_code)
   {
       // Loop through all the elements in the country-code map
       for(const auto &pair: country_names_map)
       {
            if(pair.first == country_code)
            {
                return pair.second;
            }
       }

       return "";
   }

   double distanceBetweenMetres(const double latitude_1, const double longitude_1, const double latitude_2, const double longitude_2)
   {
        double lat_1_rad = deg2Rad(latitude_1);
        double lon_1_rad = deg2Rad(longitude_1);
        double lat_2_rad = deg2Rad(latitude_2);
        double lon_2_rad = deg2Rad(longitude_2);

        double diff_lat = lat_2_rad - lat_1_rad;
        double diff_lon = lon_2_rad - lon_1_rad;

        double computation = asin(sqrt(sin(diff_lat / 2) * sin(diff_lat / 2)
                                + cos(lat_1_rad) * cos(lat_2_rad) * sin(diff_lon / 2) * sin(diff_lon / 2)));

        return 2 * earth_radius_m * computation;
   }

   double distanceBetweenMetres(const rgeocoder::Geocode &point_1, const rgeocoder::Geocode &point_2)
   {
       return distanceBetweenMetres(point_1.latitude, point_1.longitude, point_2.latitude, point_2.longitude);
   }
}