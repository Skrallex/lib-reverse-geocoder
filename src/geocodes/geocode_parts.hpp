#pragma once

#include "../../include/rgeocoder/geocode.hpp"

#include <vector>
#include <string>

namespace rgeocoder
{
    namespace parts
    {
        // Individual Geocode part files (x100)
        extern const std::vector<rgeocoder::Geocode> geocodes_part1;
        extern const std::vector<rgeocoder::Geocode> geocodes_part2;
        extern const std::vector<rgeocoder::Geocode> geocodes_part3;
        extern const std::vector<rgeocoder::Geocode> geocodes_part4;
        extern const std::vector<rgeocoder::Geocode> geocodes_part5;
        extern const std::vector<rgeocoder::Geocode> geocodes_part6;
        extern const std::vector<rgeocoder::Geocode> geocodes_part7;
        extern const std::vector<rgeocoder::Geocode> geocodes_part8;
        extern const std::vector<rgeocoder::Geocode> geocodes_part9;
        extern const std::vector<rgeocoder::Geocode> geocodes_part10;
        extern const std::vector<rgeocoder::Geocode> geocodes_part11;
        extern const std::vector<rgeocoder::Geocode> geocodes_part12;
        extern const std::vector<rgeocoder::Geocode> geocodes_part13;
        extern const std::vector<rgeocoder::Geocode> geocodes_part14;
        extern const std::vector<rgeocoder::Geocode> geocodes_part15;
        extern const std::vector<rgeocoder::Geocode> geocodes_part16;
        extern const std::vector<rgeocoder::Geocode> geocodes_part17;
        extern const std::vector<rgeocoder::Geocode> geocodes_part18;
        extern const std::vector<rgeocoder::Geocode> geocodes_part19;
        extern const std::vector<rgeocoder::Geocode> geocodes_part20;
        extern const std::vector<rgeocoder::Geocode> geocodes_part21;
        extern const std::vector<rgeocoder::Geocode> geocodes_part22;
        extern const std::vector<rgeocoder::Geocode> geocodes_part23;
        extern const std::vector<rgeocoder::Geocode> geocodes_part24;
        extern const std::vector<rgeocoder::Geocode> geocodes_part25;
        extern const std::vector<rgeocoder::Geocode> geocodes_part26;
        extern const std::vector<rgeocoder::Geocode> geocodes_part27;
        extern const std::vector<rgeocoder::Geocode> geocodes_part28;
        extern const std::vector<rgeocoder::Geocode> geocodes_part29;
        extern const std::vector<rgeocoder::Geocode> geocodes_part30;
        extern const std::vector<rgeocoder::Geocode> geocodes_part31;
        extern const std::vector<rgeocoder::Geocode> geocodes_part32;
        extern const std::vector<rgeocoder::Geocode> geocodes_part33;
        extern const std::vector<rgeocoder::Geocode> geocodes_part34;
        extern const std::vector<rgeocoder::Geocode> geocodes_part35;
        extern const std::vector<rgeocoder::Geocode> geocodes_part36;
        extern const std::vector<rgeocoder::Geocode> geocodes_part37;
        extern const std::vector<rgeocoder::Geocode> geocodes_part38;
        extern const std::vector<rgeocoder::Geocode> geocodes_part39;
        extern const std::vector<rgeocoder::Geocode> geocodes_part40;
        extern const std::vector<rgeocoder::Geocode> geocodes_part41;
        extern const std::vector<rgeocoder::Geocode> geocodes_part42;
        extern const std::vector<rgeocoder::Geocode> geocodes_part43;
        extern const std::vector<rgeocoder::Geocode> geocodes_part44;
        extern const std::vector<rgeocoder::Geocode> geocodes_part45;
        extern const std::vector<rgeocoder::Geocode> geocodes_part46;
        extern const std::vector<rgeocoder::Geocode> geocodes_part47;
        extern const std::vector<rgeocoder::Geocode> geocodes_part48;
        extern const std::vector<rgeocoder::Geocode> geocodes_part49;
        extern const std::vector<rgeocoder::Geocode> geocodes_part50;
        extern const std::vector<rgeocoder::Geocode> geocodes_part51;
        extern const std::vector<rgeocoder::Geocode> geocodes_part52;
        extern const std::vector<rgeocoder::Geocode> geocodes_part53;
        extern const std::vector<rgeocoder::Geocode> geocodes_part54;
        extern const std::vector<rgeocoder::Geocode> geocodes_part55;
        extern const std::vector<rgeocoder::Geocode> geocodes_part56;
        extern const std::vector<rgeocoder::Geocode> geocodes_part57;
        extern const std::vector<rgeocoder::Geocode> geocodes_part58;
        extern const std::vector<rgeocoder::Geocode> geocodes_part59;
        extern const std::vector<rgeocoder::Geocode> geocodes_part60;
        extern const std::vector<rgeocoder::Geocode> geocodes_part61;
        extern const std::vector<rgeocoder::Geocode> geocodes_part62;
        extern const std::vector<rgeocoder::Geocode> geocodes_part63;
        extern const std::vector<rgeocoder::Geocode> geocodes_part64;
        extern const std::vector<rgeocoder::Geocode> geocodes_part65;
        extern const std::vector<rgeocoder::Geocode> geocodes_part66;
        extern const std::vector<rgeocoder::Geocode> geocodes_part67;
        extern const std::vector<rgeocoder::Geocode> geocodes_part68;
        extern const std::vector<rgeocoder::Geocode> geocodes_part69;
        extern const std::vector<rgeocoder::Geocode> geocodes_part70;
        extern const std::vector<rgeocoder::Geocode> geocodes_part71;
        extern const std::vector<rgeocoder::Geocode> geocodes_part72;
        extern const std::vector<rgeocoder::Geocode> geocodes_part73;
        extern const std::vector<rgeocoder::Geocode> geocodes_part74;
        extern const std::vector<rgeocoder::Geocode> geocodes_part75;
        extern const std::vector<rgeocoder::Geocode> geocodes_part76;
        extern const std::vector<rgeocoder::Geocode> geocodes_part77;
        extern const std::vector<rgeocoder::Geocode> geocodes_part78;
        extern const std::vector<rgeocoder::Geocode> geocodes_part79;
        extern const std::vector<rgeocoder::Geocode> geocodes_part80;
        extern const std::vector<rgeocoder::Geocode> geocodes_part81;
        extern const std::vector<rgeocoder::Geocode> geocodes_part82;
        extern const std::vector<rgeocoder::Geocode> geocodes_part83;
        extern const std::vector<rgeocoder::Geocode> geocodes_part84;
        extern const std::vector<rgeocoder::Geocode> geocodes_part85;
        extern const std::vector<rgeocoder::Geocode> geocodes_part86;
        extern const std::vector<rgeocoder::Geocode> geocodes_part87;
        extern const std::vector<rgeocoder::Geocode> geocodes_part88;
        extern const std::vector<rgeocoder::Geocode> geocodes_part89;
        extern const std::vector<rgeocoder::Geocode> geocodes_part90;
        extern const std::vector<rgeocoder::Geocode> geocodes_part91;
        extern const std::vector<rgeocoder::Geocode> geocodes_part92;
        extern const std::vector<rgeocoder::Geocode> geocodes_part93;
        extern const std::vector<rgeocoder::Geocode> geocodes_part94;
        extern const std::vector<rgeocoder::Geocode> geocodes_part95;
        extern const std::vector<rgeocoder::Geocode> geocodes_part96;
        extern const std::vector<rgeocoder::Geocode> geocodes_part97;
        extern const std::vector<rgeocoder::Geocode> geocodes_part98;
        extern const std::vector<rgeocoder::Geocode> geocodes_part99;
        extern const std::vector<rgeocoder::Geocode> geocodes_part100;

        // Vector of all the Geocode part files
        extern const std::vector<rgeocoder::Geocode>* geocodes[100];
    }
}