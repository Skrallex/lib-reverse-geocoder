# Reverse Geocoder Lib 
Linux C++ library to produce geocode location from GPS coordinates, without requiring an internet connection.

## Installation
Note that this library is currently a work-in-progress, but you should be able to build and install it yourself if you wish.

1. Clone the repository.
2. Make sure Cmake is installed.
3. Inside the cloned directory, run `mkdir build && cd build && cmake .. && make` to build the library from source.
4. Inside the build directory, run `sudo make install` to install it to the GNU recommended location (likely `/usr/local/lib` and `/usr/local/install`).

You should then be able to include the library in your own C++ projects. See `examples/example1.cpp` for an idea of how to use it. You can also check out the [reverse-geocoder](https://gitlab.com/Skrallex/reverse-geocoder) project for a command-line application to take-in coordinates and output the closest geocode location. 

## Todo

* ~~Get .deb packaging working.~~ [done - 8c2a9edc](8c2a9edc955649b3e9215c226029c97f0f7cae99)
*  Get an apt source host setup to easily install through debian/ubuntu.
*  Remove the need to have all the individual geocode part files. Look at a better way to grab the data
    *  Possibly store it as the raw csv. Likely much slower, possibly more error prone.
    *  Compile the data into some sort of database or dat file. Not sure where these are kept on the system though.
*  Create a script to update the geocode list.
*  Use a proper search algorithm to find the closest point. At the moment, we check through every single point to find the closest. Could even sort the input data first.
